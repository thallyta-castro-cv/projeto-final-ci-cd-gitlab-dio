# Projeto Criando um Pipeline de Deploy de uma Aplicação Utilizando Gitlab, Docker e Kubernetes da formação CI/CD com gitlab da DIO

Neste projeto foi criado um pipeline de deploy de uma aplicação com dois cenários, sendo, um deploy da aplicação Node.js em um container Docker e um deploy da aplicação em um cluster Kubernetes em nuvem utilizando o GCP. Em resumo, será uma das branchs para o deploy da aplicação sendo executada em um container e outra branch com a aplicação sendo executada em um cluster Kubernetes. 

Este projeto está dividido em duas partes :
- Criando pipeline de deploy com Docker (brach: feat/docker)
- Criando pipeline de deploy com Kubernets (branch: feat/kubernets)

<b>Observação:</b> O servidor Linux na plataforma GCP não foi criado. Este projeto foi utilizado apenas como documentação para referências em possíveis funcionalidades futuras. Portanto, a pipeline de execução apresentará uma falha devido a essa ausência.

<b>Confira o passo a passo de como configurar cada projeto no README.md da branch específica.<b/>
